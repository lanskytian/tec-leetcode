package com.lst.String;
/**
 * <p>Title: CompareVersionNumbers.java</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2014</p>
 * <p>Company: IceTrail</p>
 * @author SkyLan
 * @date 2015年2月14日
 * @version 1.0
 */

/**
 * <p>Title: CompareVersionNumbers</p>
 * <p>Description: 
			Compare two version numbers version1 and version2.
			If version1 > version2 return 1, if version1 < version2 return -1, otherwise return 0.
			You may assume that the version strings are non-empty and contain only digits and the . character.
			The . character does not represent a decimal point and is used to separate number sequences.
			For instance, 2.5 is not "two and a half" or "half way to version three", 
			it is the fifth second-level revision of the second first-level revision.

			Here is an example of version numbers ordering:
			0.1 < 1.1 < 1.2 < 13.37
			</p>
 * <p>Company: IceTrail</p>
 * @author SkyLan
 * @date 2015年2月14日
 */
public class CompareVersionNumbers {

	/**
	 * <p>Title: main</p>
	 * <p>Description: </p>
	 * @data 2015年2月14日
	 * @author SkyLan
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		CompareVersionNumbers a = new CompareVersionNumbers();
		System.out.println(a.compareVersion("1.0", "1"));
	}

    public int compareVersion(String version1, String version2) {
    	String[] version1dev =  version1.split("\\.");
    	String[] version2dev =  version2.split("\\.");
    	int len1 = version1dev.length;
    	int len2 = version2dev.length;
    	if (len1 < len2) {
        	for (int i = 0; i < len1; i++) {
    			if (Integer.parseInt(version1dev[i]) > Integer.parseInt(version2dev[i])) {
    				return 1;
    			} else if (Integer.parseInt(version1dev[i]) < Integer.parseInt(version2dev[i])) {
    				return -1;
    			} else {
    				continue;
    			}
    		}
        	for (int i = len1; i < len2; i++) {
				if(Integer.parseInt(version2dev[i]) != 0)
				{
					return -1;
				}
			}
        	return 0;
		}
    	else if (len1 > len2) {
        	for (int i = 0; i < len2; i++) {
    			if (Integer.parseInt(version1dev[i]) > Integer.parseInt(version2dev[i])) {
    				return 1;
    			} else if (Integer.parseInt(version1dev[i]) < Integer.parseInt(version2dev[i])) {
    				return -1;
    			} else {
    				continue;
    			}
    		}
        	for (int i = len2; i < len1; i++) {
				if(Integer.parseInt(version1dev[i]) != 0)
				{
					return 1;
				}
			}
        	return 0;
		}
    	else {
        	for (int i = 0; i < len1; i++) {
    			if (Integer.parseInt(version1dev[i]) > Integer.parseInt(version2dev[i])) {
    				return 1;
    			} else if (Integer.parseInt(version1dev[i]) < Integer.parseInt(version2dev[i])) {
    				return -1;
    			} else {
    				continue;
    			}
    		}
        	return 0;
		}
    }
}
