package com.lst.String;


/**
 * <p>Title: ZIgZagCoversion</p>
 * <p>Description: 
		The string "PAYPALISHIRING" is written in a zigzag pattern on a given number of rows like this: 
		(you may want to display this pattern in a fixed font for better legibility)
		P   A   H   N
		A P L S I I G
		Y   I   R
		And then read line by line: "PAHNAPLSIIGYIR"
		Write the code that will take a string and make this conversion given a number of rows:
		string convert(string text, int nRows);
		convert("PAYPALISHIRING", 3) should return "PAHNAPLSIIGYIR".
 	</p>
 * <p>Company: IceTrail</p>
 * @author SkyLan
 * @date 2015年2月25日
 */ 
public class ZIgZagCoversion {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		ZIgZagCoversion a = new ZIgZagCoversion();
		System.out.println(a.convert1("PAYPALISHIRING", 3));
	}

    public String convert(String s, int nRows) {
    	String result = "";
    	int strLen = s.length();
    	System.out.println(strLen);
    	if (strLen <= nRows || nRows == 1) {
			return s;
    	}
    	int num = strLen/(2*nRows - 2);
    	
    	int r = (nRows-1)*(num+1);
    	int l = nRows;
    
    	boolean[][] mat = new boolean[r+1][l];
    	int j = 0;
    	for (int i = 0; i < r; i++) {
    		if(j==0)
    		{
        		while(j < nRows)
        		{
        			mat[i][j] = true;
//        			System.out.println("i:"+i+"k"+j);
        			j++;
        		}
        		j--;
    		}
    		mat[i+1][j-1] = true;
//    		System.out.println("i:"+(i+1)+"k"+(j-1));
    		j--;
		}
    	for (int k = 0; k < l; k++) {
    		System.out.println("k"+k);
    		for (int i = 0; i < r; i++) {
				if (mat[i][k]) {
					if(i*2+k < strLen)
					{
						System.out.println("i:"+i+"k"+k);
						System.out.println(s.charAt(i*2+k));
						result += s.charAt(i*2+k);
					}
					if(strLen != result.length())
					{
						continue;
					}
				}
			}
		}
    	return result;
    }
       
    public String convert1(String s, int nRows) {
    	String result = "";
    	int strLen = s.length();
    	if (strLen <= nRows || nRows == 1) {
			return s;
    	}
    	int n = strLen/nRows+1;
    	int m = 2*nRows-2;
    	for (int i = 0; i < nRows; i++) {
    		result += s.charAt(i);
    		for (int j = m; (j <= n*m); j += m) {
    			if ((j-i < strLen) && (i != 0) && (i != nRows-1)) {
    				result += s.charAt(j-i);
				}
    			if ((j+i < strLen) && (i != 0) && (i != nRows-1)) {
    				result += s.charAt(j+i);
				}
    			if((i == 0) && (j < strLen)){
    				result += s.charAt(j);
    			}
    			if ((i == nRows-1) && (j+i < strLen)) {
    				result += s.charAt(j+i);
				}
			}
		}
    	return result;
    }
}
