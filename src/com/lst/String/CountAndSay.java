package com.lst.String;

/*	The count-and-say sequence is the sequence of integers beginning as follows:
	1, 11, 21, 1211, 111221, ...
	1 is read off as "one 1" or 11.
	11 is read off as "two 1s" or 21.
	21 is read off as "one 2, then one 1" or 1211.
	Given an integer n, generate the nth sequence.
	Note: The sequence of integers will be represented as a string.
*/
	
/**
 * @author L.S.T
 *
 */
public class CountAndSay {
	
	public static void main(String[] args) {
		CountAndSay cas = new CountAndSay();
		System.out.println(cas.countAndSay(4));
	}
	
	/**
	 * @param nStr
	 * @return
	 */
	public String itero(String nStr) {
		String result = "";
		char temp = nStr.charAt(0);
        char read;
        int count = 1;
        for (int i = 1; i < nStr.length(); i++) {
        	read = nStr.charAt(i);
			if (read == temp) {
				count++;
			}
			else {
				result+=count;
				result+=temp;
				count=1;
				temp = read;
			}
		}
        result+=count;
		result+=temp;
        return result;
	}
	
    public String countAndSay(int n) {
    	String temp = "1";
    	for (int i = 1; i < n; i++) {
			temp = itero(temp);
		}
        return temp;
    }
    
//    int temp = n%10;
//    int left = 0;
//    int count = 1;
//    n=n/10;
//    while(n != 0)
//    {
//    	left = n%10;
//    	if (left == temp) {
//			count++;
//		}
//    	else
//    	{
//    		result = temp+ result;
//    		result = count + result;
//    		temp = left;
//    		count = 1;
//    	}
//    	n=n/10;
//    }
//    return result;
}
