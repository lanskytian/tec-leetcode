package com.lst.BitManipulation;
/**
 * <p>Title: Single_Number.java</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2014</p>
 * <p>Company: IceTrail</p>
 * @author SkyLan
 * @date 2015年2月15日
 * @version 1.0
 */ 

import java.util.HashMap;
import java.util.Map;

/**
 * <p>Title: Single_Number</p>
 * <p>Description: 
		Given an array of integers, every element appears twice except for one.
		Find that single one.
		Note:
		Your algorithm should have a linear runtime complexity. Could you implement it without using extra memory?</p>
 * <p>Company: IceTrail</p>
 * @author SkyLan
 * @date 2015年2月15日
 */
public class SingleNumber {

	/**
	 * <p>Title: main</p>
	 * <p>Description: </p>
	 * @data 2015年2月15日
	 * @author SkyLan
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		SingleNumber a= new SingleNumber();
		int[] b = {2,3,4,5,6,7,8,9,2,3,4,5,6,8,9};
		System.out.println(a.singleNumber(b));
	}

    public int singleNumber(int[] A) {
    	Map<Integer, Integer> a = new HashMap<Integer, Integer>();
    	for (int i = 0; i < A.length; i++) {
    		if(a.containsKey(A[i]))
    		{
    			a.put(A[i], a.get(A[i]) + 1);
    		}
    		else {
				a.put(A[i], 1);
			}
    		if (a.get(A[i]) == 2) {
				a.remove(A[i]);
			}
		}    	
    	for(int sub : a.keySet())
    	{
    		return sub;
    	}
    	return 0;
    }
    
    public int singleNumber1(int[] A) {
    	int sum = 0;
    	for (int i = 0; i < A.length; i++) {
			sum ^= A[i];
		}
    	return sum;
    }
}
