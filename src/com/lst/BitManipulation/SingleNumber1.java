package com.lst.BitManipulation;
import java.util.HashMap;
import java.util.Map;

/**
 * <p>Title: SingleNumber1.java</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2014</p>
 * <p>Company: IceTrail</p>
 * @author SkyLan
 * @date 2015年2月17日
 * @version 1.0
 */

/**
 * <p>Title: SingleNumber1</p>
 * <p>Description: </p>
 * <p>Company: IceTrail</p>
 * @author SkyLan
 * @date 2015年2月17日
 */
public class SingleNumber1 {

	/**
	 * <p>Title: main</p>
	 * <p>Description: </p>
	 * @data 2015年2月17日
	 * @author SkyLan
	 * @param args
	 */

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		SingleNumber1 a = new SingleNumber1();
		int[] b = {5, 5, 6, 6, 6, 5};
//				87,56,87,56,34,3,98,35,87,3,34,35,3,34,35};
//		System.out.println(a.singleNumber1(b));
		a.singleNumber1(b);
	}

    public int singleNumber(int[] A) {
    	Map<Integer, Integer> a = new HashMap<Integer, Integer>();
    	for (int i = 0; i < A.length; i++) {
    		if(a.containsKey(A[i]))
    		{
    			a.put(A[i], a.get(A[i]) + 1);
    		}
    		else {
				a.put(A[i], 1);
			}
    		if (a.get(A[i]) == 3) {
				a.remove(A[i]);
			}
		}    	
    	for(int sub : a.keySet())
    	{
    		return sub;
    	}
    	return 0;
    }
    
    public int singleNumber1(int[] A) {
    	int one = 0, two = 0, three = 0;
        for(int i = 0; i < A.length; i++) {
            two |= one & A[i];
//            System.out.println(one & A[i]);
//            System.out.println(two);
            one ^= A[i];
//            System.out.println(one);
            three = one & two;
//            System.out.println(three);
            one &= ~three;
//            System.out.println(~three);
//            System.out.println(one);
            two &= ~three;
            System.out.println("one:" + one + "   two:" + two + "    three:" + three);
            System.out.println("-------------");
        }
        return one;
    }
}
