package com.lst.Tree;
/**
 * <p>Title: SameTree.java</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2014</p>
 * <p>Company: IceTrail</p>
 * @author SkyLan
 * @date 2015年2月25日
 * @version 1.0
 */

/**
 * <p>Title: SameTree</p>
 * <p>Description: 
		Given two binary trees,
		write a function to check if they are equal or not.
		Two binary trees are considered equal 
		if they are structurally identical and the nodes have the same value.</p>
 * <p>Company: IceTrail</p>
 * @author SkyLan
 * @date 2015年2月25日
 */
public class SameTree {
	
	public class TreeNode {
		int val;
		TreeNode left;
		TreeNode right;

		TreeNode(int x) {
			val = x;
		}
	}

	/**
	 * <p>Title: main</p>
	 * <p>Description: </p>
	 * @data 2015年2月25日
	 * @author SkyLan
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		System.out.println(true&true);
	}

	public boolean isSameTree(TreeNode p, TreeNode q) {
		if (p == null && q == null) {
			return true;
		}
		else {
			if (p != null && q != null) {
				if (p.val != q.val) {
					return false;
				}
				return isSameTree(p.left, q.left)&isSameTree(p.right, q.right);
			}
			return false;
		}
	}
}
