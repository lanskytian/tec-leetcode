package com.lst.Tree;

/**
 * @author Administrator
 *
 */
public class UniqueBinarySearchTrees {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		UniqueBinarySearchTrees a = new UniqueBinarySearchTrees();
		System.out.println(a.numTrees(6));
	}

    int numTrees(int n) {
        if (n == 1 || n == 0) {
        	return 1;
		}
        int sum = 0;
        for (int i = 0; i < n; i++) {
        	sum += numTrees(i)*numTrees(n-1-i);
		}
        return sum;
    }
}
