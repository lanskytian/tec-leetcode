package com.lst.Tree;

public class MaxDepth {

	public class TreeNode {
		int val;
		TreeNode left;
		TreeNode right;

		TreeNode(int x) {
			val = x;
		}
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}
	
	public int maxDepth(TreeNode root) {
		if(root == null)
			return 0;
		int l = maxDepth(root.left) + 1;
		int r = maxDepth(root.right) + 1;
		return l<r?r:l;
	}
}
