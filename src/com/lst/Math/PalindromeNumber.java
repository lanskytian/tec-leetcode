package com.lst.Math;

/**
 * @author Administrator
 *
 */
public class PalindromeNumber {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		PalindromeNumber pn = new PalindromeNumber();
		System.out.println(pn.isPalindrome1(1555555551));
	}
	
    public boolean isPalindrome(int x) {
    	int temp = x;
    	int result = 0;
    	while (temp > 0) {
			result = result*10 + temp%10; 
			temp /= 10;
		}
    	if (x == result) {
    		return true;
		}
    	return false;
    }
    
    public boolean isPalindrome1(int x) {
    	String temp = String.valueOf(x);
    	int len = temp.length();
    	for (int i = 0; i < len/2; i++) {
			if (temp.charAt(i) != temp.charAt(len-1-i)) {
				return false;
			}
		}
    	return true;
    }
}
