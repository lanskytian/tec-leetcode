package com.lst.Math;
/**
 * <p>Title: ReserveInt.java</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2014</p>
 * <p>Company: IceTrail</p>
 * @author SkyLan
 * @date 2015年2月14日
 * @version 1.0
 */


/**
 * <p>Title: ReserveInt</p>
 * <p>Description: 
		Reverse digits of an integer.
		Example1: x = 123, return 321
		Example2: x = -123, return -321</p>
 * <p>Company: IceTrail</p>
 * @author SkyLan
 * @date 2015年2月14日
 */
public class ReserveInt {
	/**
	 * <p>Title: main</p>
	 * <p>Description: </p>
	 * @data 2015年2月14日
	 * @author SkyLan
	 * @param args
	 */

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		ReserveInt a = new ReserveInt();
		System.out.println(a.reverse(1534236469));
	}
	
	public long reverse(long x) {
		String resCopy = "";
		String xCopy = "";
		if (x<0) {
			resCopy += '-';
			xCopy = String.valueOf(-x);
		}
		else {
			xCopy = String.valueOf(x);
		}
		int len = xCopy.length();
		
		for (int i = len-1; i >= 0; i--) {
			resCopy += xCopy.charAt(i);
		}
		long result = Long.parseLong(resCopy);
		if (result> java.lang.Math.pow(2,31)-1 || result<= -java.lang.Math.pow(2,31)) {
			return 0;
		}
		return result;
	}
}
