package com.lst.HashMap;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;

/**
 * <p>Title: Num3Sum.java</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2014</p>
 * <p>Company: IceTrail</p>
 * @author SkyLan
 * @date 2015年2月22日
 * @version 1.0
 */

/**
 * <p>Title: Num3Sum</p>
 * <p>Description:</p>
 * <p>Company: IceTrail</p>
 * @author SkyLan
 * @date 2015年2月22日
 */
public class ThreeSum {

	/**
	 * <p>Title: main</p>
	 * <p>Description:</p>
	 * @data 2015年2月22日
	 * @author SkyLan
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		ThreeSum sd = new ThreeSum();
		int[] b = {7,-1,14,-12,-8,7,2,-15,8,8,-8,-14,-4,-5,7,9,11,-4,-15,-6,1,-14,4,3,10,-5,2,1,6,11,2,-2,-5,-7,-6,2,-15,11,-6,8,-4,2,1,-1,4,-6,-15,1,5,-15,10,14,9,-8,-6,4,-6,11,12,-15,7,-1,-9,9,-1,0,-4,-1,-12,-2,14,-9,7,0,-3,-4,1,-2,12,14,-10,0,5,14,-1,14,3,8,10,-8,8,-5,-2,6,-11,12,13,-7,-12,8,6,-13,14,-2,-5,-11,1,3,-6};
		System.out.println(sd.threeSum(b));
	}

	public ArrayList<List<Integer>> threeSum(int[] num) {
//		Long a = System.currentTimeMillis();
		Arrays.sort(num);
		Map<Integer, Integer> singleNum = new HashMap<Integer, Integer>();
		for (int i = 0; i < num.length; i++) {
			singleNum.put(num[i], i);
		}
		HashSet<List<Integer>> result = new HashSet<List<Integer>>();
		for (int i = 0; i < num.length; i++) {
			if (i != 0 && num[i] == num[i-1]) {
				continue;
			}
			for (int j = i + 1; j < num.length; j++) {
				int key = 0 - num[i] - num[j];
				if (singleNum.containsKey(key) && singleNum.get(key) != i && singleNum.get(key) != j) {
					List<Integer> listSub = new ArrayList<Integer>();
					if (key > num[j]) {
						listSub.add(num[i]);
						listSub.add(num[j]);
						listSub.add(key);
					}
					else if(key < num[i]){
						listSub.add(key);
						listSub.add(num[i]);
						listSub.add(num[j]);
					}
					else{
						listSub.add(num[i]);
						listSub.add(key);
						listSub.add(num[j]);
					}
					result.add(listSub);
				}
			}
		}
//		Long b = System.currentTimeMillis();
//		System.out.println(b-a);
		return new ArrayList<List<Integer>>(result);
	}

	public void threeSum2(int[] num) {
		Map<Integer, List<List<Integer>>> a = new HashMap<Integer, List<List<Integer>>>();
		List<List<Integer>> result = new ArrayList<List<Integer>>();
		for (int i = 0; i < num.length; i++) {
			for (int j = i + 1; j < num.length; j++) {
				int key = num[i] + num[j];
				if (a.containsKey(key)) {
					List<Integer> listSub = new ArrayList<Integer>();
					listSub.add(num[i]);
					listSub.add(num[j]);
					a.get(key).add(listSub);
				} else {
					List<List<Integer>> list = new ArrayList<List<Integer>>();
					List<Integer> listSub = new ArrayList<Integer>();
					listSub.add(num[i]);
					listSub.add(num[j]);
					list.add(listSub);
					a.put(key, list);
				}
				System.out.println(a);
			}
		}

		for (int i = 0; i < num.length; i++) {
			if (a.containsKey(0 - num[i])) {
				for (int j = 0; j < a.get(0 - num[i]).size(); j++) {
					a.get(0 - num[i]).get(j).add((num[i]));
					result.add(a.get(0 - num[i]).get(j));
					a.get(0 - num[i]).remove(j);
				}
			}
		}
		System.out.println(result);
	}
}
