package com.lst.HashMap;

import java.util.HashMap;
import java.util.Map;

/*
	Given an array of integers, find two numbers such that they add up to a specific target number.
	The function twoSum should return indices of the two numbers such that they add up to the target, 
	where index1 must be less than index2. Please note that your returned answers (both index1 and index2) are not zero-based.
	You may assume that each input would have exactly one solution.
	Input: numbers={2, 7, 11, 15}, target=9
	Output: index1=1, index2=2
*/

public class TwoSum {
	
	public static void main(String[] args) {
	// TODO Auto-generated method stub
		TwoSum a = new TwoSum();
		int[] b = {5,7,8,8};
		int[] c = a.twoSum2(b, 15);
		for (int i = 0; i < c.length; i++) {
			System.out.println("index"+ c[i]);
		}
	}
	
	public int[] twoSum(int numbers[], int target) {
		int[] result = new int[2];
		for (int i = 0; i < numbers.length; i++) {
			for (int j = i+1; j < numbers.length; j++) {
				if ((numbers[i] + numbers[j] == target)  && (numbers[i] != 0)) {
					if (numbers[i] < numbers[j]) {
						result[0] = i+1;
						result[1] = j+1;
					}
					else {
						result[1] = i+1;
						result[0] = j+1;
					}
				}
			}
		}
		return result;
	}

	public int[] twoSum2(int numbers[], int target) {
		int[] result = new int[2];
		Map<Integer, Integer> map = new HashMap<Integer, Integer>();
		for (int i = 0; i < numbers.length; i++) {
			if(map.containsKey(target - numbers[i])){
				result[0] = map.get(target - numbers[i]);
				result[1] = i + 1;
			}
			map.put(numbers[i], i+1);
		}
		return result;
	}
}
